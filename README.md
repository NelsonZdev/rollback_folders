# rollback_folders
Simple program to rollback your documents, before being modified by a CI program.
You can keep a number of copies before the program automatically deletes them. By default it is 1.

## You can add files and folders to modify (delete, save, etc)
    - Mofifies:
        FILES_TO_MODIFY['your_file', 'your_file_2']
        FOLDERS_TO_MODIFY['your_folder', 'your_folder_2']


## You can modify the number from:
    - "remove (rollbackPath, mylist, -numberOfCopies)"

## To change the target directory change:
    CONTAINER_PATH = os.path.normpath(os.getcwd() + os.sep + os.pardir) for
    CONTAINER_PATH = "Your path" -Ejem "/home/[jourName]/target" or "C://target" (**not tested with windows**)

## Thought structure:
```
root/
│
└───your_jenkins_home_folder/
    │
    └───your_project/
    │   │   **this_program**/
    │   │   your_proyect_folders/
    │   │   your_proyect_files
    │   │   ...
    │
    └───rollbacks_out/ (rollbacks performed by this program)
        │   your_proyect_folders_back/
        │   your_proyect_files_back
        │   ...
```
## In linux you can run the program: 
    - With the command **./run.sh [folder_name]** or **sh run.sh [folder_name]** into a your proyect folder

## On other operating systems run:
    - this_project_folder/**python src/app.py [folder_name]**
### or
    - **python [full path]/your_project/this_project/src/app.py [folder_name]** 

----
### If you want to restore your files use [restore_project_from_backup](https://gitlab.com/NelsonZdev/restore_project_from_backup)
----

### Thought for jenkins CI.

----

## Use it as you wish - Nelson Zapata :)
