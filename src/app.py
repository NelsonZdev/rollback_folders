import os
import time
import pipes
import sys
import shutil

# Directory before python project
CONTAINER_PATH = os.path.normpath(os.getcwd() + os.sep + os.pardir)
BACKUP_PATH = CONTAINER_PATH + "/"

FILES_TO_MODIFY = [
    'INSERT YOUR FILES TO SAVE'
]
FOLDERS_TO_MODIFY = [
    'target',
    'dist'
]


def proyectName():
    args = list(sys.argv)
    if (len(args) == 1):
        raise FileNotFoundError("The project name has not been set in args")
    else:
        if (len(args) > 2):
            print("Just need only 1 argument.\nOther args has been ignored")
        return args[1]


def newPath(name):
    try:
        os.stat(name)
    except FileNotFoundError:
        os.mkdir(name)


def backup(destiny):
    folderByProyect = CONTAINER_PATH + "/" 
    # Folders
    for index in FOLDERS_TO_MODIFY:                         # Folders marked
        print(folderByProyect + index)
        if os.path.isdir(folderByProyect + index):          # Exist folders ?
            shutil.copytree(folderByProyect + index, destiny + "/" + index)
    # Files
    for index in FILES_TO_MODIFY:                           # Files marked
        if os.path.isfile(folderByProyect + index):    # Exist files ?
            shutil.copyfile(folderByProyect + index, destiny + "/" + index)

# Remove older rollbacks


def remove(destiny, arrayFolders, numberOfRollBacks=1):
    if (len(arrayFolders) > numberOfRollBacks):            # Count of folders
        # Min index of folders to save
        foldersToSave = len(arrayFolders) - numberOfRollBacks
        folderIndex = 0                                     # Index
        for index in arrayFolders:
            folderIndex += 1
            if os.path.isdir(destiny):                      # Exist folder?
                if (folderIndex <= foldersToSave):           # Can't remove updated folders
                    shutil.rmtree(destiny + "/" + index)    # Remove folder


def main():
    rollbackPath = BACKUP_PATH + "backup_" + proyectName()
    # BackupFolder
    newPath(rollbackPath)
    # New folder
    actualTime = time.strftime('%Y%m%d-%H%M%S')  # Actual date
    timePath = rollbackPath + '/' + actualTime  # Path + actual date
    newPath(timePath)
    # list of folders in current directory
    listOfRollbackPath = sorted(os.listdir(rollbackPath))
    # Create new backups
    backup(timePath)
    # Remove Older backups
    remove(rollbackPath, listOfRollbackPath, 1)


if __name__ == "__main__":
    main()
