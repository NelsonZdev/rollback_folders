#!/bin/bash

# Actual Folder
DIR=$(pwd)

# Script Folder
cd $(dirname $(readlink -f $0))
python3 src/app.py $1

# Return origin folder
cd $DIR